//
// Created by m-ochi on 16/03/15.
//

#include "networkall.h"
#include <string>

#define MAX_STRING 100

NetworkAll::NetworkAll(std::vector<std::string> net_files, std::string mode) : Network() {

    // property
    max_num_vertices    = 1000;
    num_vertices = 0;
    num_edges    = 0;

    vertices    = std::vector<std::unique_ptr<Vertex>>(max_num_vertices);
    hash_table_size = 30000000;

    for(int i=0; i<max_num_vertices; i++ ) {
        std::unique_ptr<Vertex> v(new Vertex());
        this->vertices[i] = std::move(v);
    }

    vertex_hash_table = std::vector<int>(hash_table_size);
    for (int k=0; k<hash_table_size; k++) {
        vertex_hash_table[k] = -1;
    }

    // property
    max_num_vertices    = 1000;
//    max_num_edges       = 100000000;
    num_vertices = 0;
    num_edges    = 0;

    hash_table_size = 30000000;

    vertices    = std::vector<std::unique_ptr<Vertex>>(max_num_vertices);
    for(int i=0; i<max_num_vertices; i++ ) {
        std::unique_ptr<Vertex> v(new Vertex());
        this->vertices[i] = std::move(v);
    }

    vertex_hash_table = std::vector<int>(hash_table_size);
    for (int k=0; k<hash_table_size; k++) {
        vertex_hash_table[k] = -1;
    }

    // init
    readFromFile(net_files, mode);

}

NetworkAll::~NetworkAll() {
}

void NetworkAll::readFromFile( std::vector<std::string> net_files, std::string mode ) {

    int N = net_files.size();

    FILE *fin;
    char name_v1[MAX_STRING];
    char name_v2[MAX_STRING];
    char str[2 * MAX_STRING + 10000];
    int vid;
    double weight;

    std::unique_ptr<int64_t[]> num_edgeses(new int64_t[N]);

    for(int i=0; i<N; i++){
        std::string net_file = net_files[i];
        num_edgeses[i] = 0;

        fin = fopen(net_file.c_str(), "rb");
        if (fin == NULL) {
            printf("ERROR: network file not found!\n");
            exit(1);
        }

        //fgets:ファイルポインタから１行読み込む
        while (fgets(str, sizeof(str), fin)) {
            (num_edgeses[i])++;
        }

        fclose(fin);

    }

    this->num_edges = 0;
    for(int i=0; i<N; i++){
        this->num_edges = this->num_edges + num_edgeses[i];
    }

    std::cout << "Number of edges " << mode << ": " << this->num_edges << std::endl;

    this->edges = std::vector<std::unique_ptr<Edge>>(this->num_edges);

    for(int i=0; i<this->num_edges; i++) {
        std::unique_ptr<Edge> e(new Edge());
        this->edges[i] = std::move(e);
    }

    int64_t amount_e = 0;
    for(int i=0; i<N; i++){
        std::string net_file = net_files[i];

        fin = fopen(net_file.c_str(), "rb");
        for (int k = 0; k < num_edgeses[i]; k++) {
            fscanf(fin, "%s %s %lf", name_v1, name_v2, &weight);

            if (amount_e % 10000 == 0) {
                printf("Reading edges %s: %.3lf%%%c", mode.c_str(), amount_e / (double)(this->num_edges + 1) * 100, 13);
                fflush(stdout);
            }

            vid = this->searchHashTable(name_v1);
            if (vid == -1) {
                vid = this->addVertex(name_v1);
            }

            this->vertices[vid]->degree += weight;
            this->edges[amount_e]->source_id = vid;

            vid = this->searchHashTable(name_v2);
            if (vid == -1) {
                vid = this->addVertex(name_v2);
            }

            this->vertices[vid]->degree += weight;
            this->edges[amount_e]->target_id = vid;

            this->edges[amount_e]->weight = weight;

            amount_e++;
        }
        fclose(fin);

    }

    printf("Number of vertices %s: %d          \n", mode.c_str(), this->num_vertices);

}


/*
int main() {

    std::vector<std::string> networkfiles;
    networkfiles.push_back("dataminimal/st-st.csv");
    networkfiles.push_back("dataminimal/st-corp.csv");
    networkfiles.push_back("dataminimal/st-imp.csv");
    std::unique_ptr<NetworkAll> networkAll(new NetworkAll(networkfiles));

    std::cout << "networkAll->num_vertices:"    << networkAll->num_vertices     << std::endl;

    for(int i=0; i<networkAll->num_vertices; i++) {
        std::cout << "networkAll->vertices[" << i << "]->degree:"    << networkAll->vertices[i]->degree     << std::endl;
        std::cout << "networkAll->vertices[" << i << "]->name:"    << networkAll->vertices[i]->name     << std::endl;

    }
    std::cout << "" << std::endl;

    for(int i=0; i<networkAll->num_edges; i++) {
        std::cout << "networkAll->edges[" << i << "]->source_id:"    << networkAll->edges[i]->source_id     << std::endl;
        std::cout << "networkAll->edges[" << i << "]->target_id:"    << networkAll->edges[i]->target_id     << std::endl;
        std::cout << "networkAll->edges[" << i << "]->weight:"    << networkAll->edges[i]->weight     << std::endl;

    }


    int v_id = networkAll->searchHashTable((std::string)"\"0669\"");
    std::cout << "networkAll vertex 0669 id:"    << v_id    << std::endl;


    return 0;
}
*/
