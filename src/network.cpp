//
// Created by m-ochi on 16/03/11.
//

#include "network.h"

#define MAX_STRING 100

Network::Network(std::string net_file, std::string mode) {

    // property
    max_num_vertices    = 1000;
//    max_num_edges       = 100000000;
    num_vertices = 0;
    num_edges    = 0;

    hash_table_size = 30000000;

    vertices    = std::vector<std::unique_ptr<Vertex>>(max_num_vertices);
    for(int i=0; i<max_num_vertices; i++ ) {
        std::unique_ptr<Vertex> v(new Vertex());
        this->vertices[i] = std::move(v);
    }

    vertex_hash_table = std::vector<int>(hash_table_size);
    for (int k=0; k<hash_table_size; k++) {
        vertex_hash_table[k] = -1;
    }

    // init
    readFromFile(net_file, mode);

}

Network::Network() {}

Network::~Network() {
}

void Network::readFromFile( std::string net_file, std::string mode ) {
    FILE *fin;
    char name_v1[MAX_STRING];
    char name_v2[MAX_STRING];
    char str[2 * MAX_STRING + 10000];
    int vid;
    double weight;

    fin = fopen(net_file.c_str(), "rb");
    if (fin == NULL) {
        printf("ERROR: network file not found!\n");
        exit(1);
    }

    this->num_edges = 0;

    //fgets:ファイルポインタから１行読み込む
    while (fgets(str, sizeof(str), fin)) {
        (this->num_edges)++;
    }

    fclose(fin);
    std::cout << "Number of edges " << mode << ": " << this->num_edges << std::endl;

    this->edges = std::vector<std::unique_ptr<Edge>>(this->num_edges);
    for(int i=0; i<this->num_edges; i++) {
        std::unique_ptr<Edge> e(new Edge());
        this->edges[i] = std::move(e);
    }

    fin = fopen(net_file.c_str(), "rb");
    for (int k = 0; k < this->num_edges; k++) {
        fscanf(fin, "%s %s %lf", name_v1, name_v2, &weight);

        if (k % 10000 == 0) {
            printf("Reading edges %s: %.3lf%%%c", mode.c_str(), k / (double)(this->num_edges + 1) * 100, 13);
            fflush(stdout);
        }

        vid = this->searchHashTable(name_v1);
        if (vid == -1) {
            vid = this->addVertex(name_v1);
        }

        this->vertices[vid]->degree += weight;
        this->edges[k]->source_id = vid;

        vid = this->searchHashTable(name_v2);
        if (vid == -1) {
            vid = this->addVertex(name_v2);
        }

        this->vertices[vid]->degree += weight;
        this->edges[k]->target_id = vid;

        this->edges[k]->weight = weight;
    }
    fclose(fin);

    printf("Number of vertices %s: %d          \n", mode.c_str(), this->num_vertices);

}

int Network::searchHashTable(std::string key) {

//    std::cout << "key:" << key << std::endl;

    u_int addr = this->Hash(key);

//    std::cout << "addr:" << addr << std::endl;

    while (1) {
        if (this->vertex_hash_table[addr] == -1) {
            return -1;
        }

        int v_addr_id = this->vertex_hash_table[addr];
        if ((std::string)key == this->vertices[v_addr_id]->name) {
            return v_addr_id;
        }
        addr = (addr + 1) % this->hash_table_size;
    }
//    return -1;
}

/* Add a vertex to the vertex set */
int Network::addVertex(char* name) {
    size_t length = strlen(name) + 1;
    if (length > MAX_STRING) {
        length = MAX_STRING;
    }

    vertices[this->num_vertices]->name   = name;
    vertices[this->num_vertices]->degree = 0;
    this->num_vertices++;
    if (this->num_vertices + 2 >= this->max_num_vertices) {
        this->max_num_vertices += 1000;
        vertices.resize(this->max_num_vertices);
    }
    insertHashTable(name, this->num_vertices - 1);
    return this->num_vertices - 1;
}

void Network::insertHashTable(char *key, int new_v_id) {
    int addr = Hash(key);
    while (this->vertex_hash_table[addr] != -1) {
        addr = (addr + 1) % this->hash_table_size;
    }

    this->vertex_hash_table[addr] = new_v_id;
}

// hash * seed + (*key++)の部分は，*keyを文字コードにキャストしたもので演算子、keyのアドレスを次に進める
u_int Network::Hash(std::string skey) {
    unsigned int seed = 131;
    unsigned int hash = 0;
    const char* key = skey.c_str();
    while (*key) {
        hash = hash * seed + (*key++);
    }
    hash = hash % hash_table_size;
    return (u_int)hash;
}

/*
int main() {

    std::unique_ptr<Network> network( new Network("dataminimal/st-st.csv", "ww") );

    std::cout << "network->num_vertices:"    << network->num_vertices     << std::endl;
    std::cout << "network->num_edges:"    << network->num_edges     << std::endl;

    for(int i=0; i<network->num_vertices; i++) {
        std::cout << "network->vertices[" << i << "]->degree:"    << network->vertices[i]->degree     << std::endl;
        std::cout << "network->vertices[" << i << "]->name:"    << network->vertices[i]->name     << std::endl;

    }
    std::cout << "" << std::endl;

    for(int i=0; i<network->num_edges; i++) {
        std::cout << "network->edges[" << i << "]->source_id:"    << network->edges[i]->source_id     << std::endl;
        std::cout << "network->edges[" << i << "]->target_id:"    << network->edges[i]->target_id     << std::endl;
        std::cout << "network->edges[" << i << "]->weight:"    << network->edges[i]->weight     << std::endl;

    }


    int v_id = network->searchHashTable((std::string)"\"0669\"");
    std::cout << "network vertex 0669 id:"    << v_id    << std::endl;

    return 0;
}
*/
