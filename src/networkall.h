//
// Created by m-ochi on 16/03/15.
//

#ifndef PTESELF_NETWORKALL_H
#define PTESELF_NETWORKALL_H

#include "network.h"

class NetworkAll : public Network
{
public:
    NetworkAll(std::vector<std::string> net_files, std::string mode="all");
    ~NetworkAll();

private:
    void readFromFile( std::vector<std::string> net_files, std::string mode );


};


#endif //PTESELF_NETWORKALL_H
