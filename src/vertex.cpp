//
// Created by m-ochi on 16/03/11.
//

#include "vertex.h"
#include <iostream>
#include <vector>
//#include <memory>

Vertex::Vertex() {
    degree =0;
    name = "";
}

Vertex::~Vertex() {
}

/*
int main() {

//    std::unique_ptr<Vertex> v( new Vertex() );
//    v->degree += (double)100;
//    v->name = (std::string)"NiceV";

//    std::cout << "v->degree:"   << v->degree    << std::endl;
//    std::cout << "v->name:"     << v->name      << std::endl;

//    std::vector<Vertex>* vn = new std::vector<Vertex>(3);
//    std::vector<Vertex*> vn(10);
//    std::unique_ptr<std::vector<Vertex>> vn( new std::vector<Vertex>(10));
//    std::unique_ptr<std::vector<Vertex>> vn( new std::vector<Vertex>(10));
//    std::vector<std::unique_ptr<Vertex>> vn( new Vertex() )(10);
//    std::vector<std::shared_ptr<Vertex>> vn(10);
//    std::vector<std::unique_ptr<Vertex>> vn(10, new Vertex());
//    std::vector<std::unique_ptr<Vertex>> vn;
    std::vector<std::unique_ptr<Vertex>> vn(10);
//    std::vector<std::unique_ptr<Vertex>>* vn = new std::vector<std::unique_ptr<Vertex>>(10);
    for(int i=0; i<10; i++){
//        std::shared_ptr<Vertex> v(new Vertex() );
//        std::shared_ptr<Vertex> v(new Vertex() );
        std::unique_ptr<Vertex> v(new Vertex() );
//        Vertex* v = new Vertex();
        v->degree = i;
        v->name = "hello";
//        vn.push_back(v);
//        vn[i] = i;
        vn[i] = std::move(v);
//        vn[i] = v;
//        vn.emplace_back(v);
//        Vertex lv = vn[i];
//        lv->degree = i+1;
//        vn[i].degree = i+1;
//        vn[i]->degree = i+1;
//        vn[i]->name = "hello";
//        vn[i];
    }
//    delete vn;

    vn.resize(100);

    for(int i=0; i<10; i++){
        std::cout << "vn[i]->degree:"   << vn[i]->degree    << std::endl;
        std::cout << "vn[i]->name:"     << vn[i]->name      << std::endl;
    }

    return 0;
}
 */

