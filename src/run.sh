#!/bin/sh

cmake .
make clean
make

./train_pte \
  --train_ww=../dataminimal/st-st.csv \
  --train_wd=../dataminimal/st-corp.csv \
  --train_wl=../dataminimal/st-imp.csv \
  --output=vec_2nd_wo_norm.txt \
  --binary=0 \
  --size=2 \
  --order=2 \
  --negative=5 \
  --samples=1 \
  --threads=1 \
  --rho=0.05

