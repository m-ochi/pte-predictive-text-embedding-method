//
// Created by m-ochi on 16/03/11.
//    PTE初期化時に指定するパラメータ群の値（プログラム実行中に変化しない）
//

#ifndef PTESELF_CONSTANTSETTINGS_H
#define PTESELF_CONSTANTSETTINGS_H

#include <iostream>
#include <memory>
#include <cstdint>

class ConstantSettings {

public:
    ConstantSettings(
        std::string ww_net_a,
        std::string wd_net_a,
        std::string wl_net_a,
        std::string outputfile_a,
        int binary_a=0,
        int dim_a=100,
        int order_a=2,
        int negative_a=5,
        int samples_a=1,
        int threads_a=1,
        float rho_a=0.025
    );

    ~ConstantSettings();

    std::string ww_net;
    std::string wd_net;
    std::string wl_net;
    std::string outputfile;
    int binary;
    int dim;
    int order;
    int negative;
    int samples;
    int threads;
    float rho;

    int hash_table_size;
    int neg_table_size;
    int sigmoid_table_size;

private:

};



#endif //PTESELF_CONSTANTSETTINGS_H
