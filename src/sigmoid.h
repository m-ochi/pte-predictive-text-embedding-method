//
// Created by m-ochi on 16/03/16.
//

#ifndef PTESELF_SIGMOID_H
#define PTESELF_SIGMOID_H

#include <memory>

typedef float real;                    // Precision of float numbers

class Sigmoid {

public:
    Sigmoid(int sigmoid_table_size_a=1000, int sigmoidbound_a=6);
    ~Sigmoid();
    real fastFunc(real x);

private:
    int sigmoid_table_size;
    real sigmoidbound;
    std::unique_ptr<real[]> sigmoid_table;
};


#endif //PTESELF_SIGMOID_H
