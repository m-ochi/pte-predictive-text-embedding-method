//
// Created by m-ochi on 16/03/11.
//

#include "edge.h"


Edge::Edge() {
    source_id =0;
    target_id =0;
    weight = 0.0;
}

Edge::~Edge() {
}

/*
int main() {

    std::unique_ptr<Edge> e( new Edge() );
    e->source_id = 100;
    e->target_id = 200;

    std::cout << "e->source_id:"    << e->source_id     << std::endl;
    std::cout << "e->target_id:"    << e->target_id     << std::endl;
    std::cout << "e->weight:"       << e->weight        << std::endl;

    return 0;
}
*/

