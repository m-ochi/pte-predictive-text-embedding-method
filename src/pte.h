#ifndef INCLUDED_pte_h_
#define INCLUDED_pte_h_

#include <iostream>
#include <string>
#include <cstdint>
#include <thread>
#include <memory>
#include "aliasmethod/aliastable.h"
#include "network.h"
#include "networkall.h"
#include "samplers.h"
#include "embeddedvector.h"
#include "constantsettings.h"
#include "sigmoid.h"
#include <random>

typedef float real;                    // Precision of float numbers

class PTE {
protected:

    // 定数群
    static std::unique_ptr<ConstantSettings> constant_settings;

    static std::unique_ptr<Network> network_ww;
    static std::unique_ptr<Network> network_wd;
    static std::unique_ptr<Network> network_wl;
    static std::unique_ptr<Network> network_all;

    static std::unique_ptr<Samplers> sampler_ww;
    static std::unique_ptr<Samplers> sampler_wd;
    static std::unique_ptr<Samplers> sampler_wl;

    static std::unique_ptr<EmbeddedVector> emb_ww;
    static std::unique_ptr<EmbeddedVector> emb_wd;
    static std::unique_ptr<EmbeddedVector> emb_wl;
    static std::unique_ptr<EmbeddedVector> emb_all;

    static std::unique_ptr<Sigmoid> sigmoid;

    //method
    static void* learnVector(Network* network_each, EmbeddedVector* emb_each, Samplers* sampler_each, Network* network_all_a, EmbeddedVector* emb_all_a, uint64_t seed);

    static int64_t total_samples;
    static int64_t current_sample_count;
    static real cur_rho;

    static void update(std::vector<real>& vec_u, std::vector<real>& vec_v, std::vector<real>& vec_error, int label);
    static void trainPTEThread(int id);

    virtual void output();
    static void output_tmp(int loop_c);
    static void disp_vec(std::vector<real>);

public:
    PTE(
        std::string ww_net_a,
        std::string wd_net_a,
        std::string wl_net_a,
        std::string outputfile_a,
        int binary_a=0,
        int size_a=100,
        int order_a=2,
        int negative_a=5,
        int samples_a=1,
        int threads_a=1,
        float rho_a=0.025
    );
    ~PTE();

    virtual void run();

};

#endif
