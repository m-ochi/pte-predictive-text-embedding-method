//
// Created by m-ochi on 16/03/11.
// Networkの特徴量の格納(学習するベクトルデータは別)
//

#ifndef PTESELF_NETWORK_H
#define PTESELF_NETWORK_H

#include <iostream>
#include <memory>
#include <vector>
#include <string.h>
#include <cstdint>
#include "vertex.h"
#include "edge.h"

class Network {
public:
    //
    Network(std::string net_file, std::string mode);
    Network();
    ~Network();
    // property
    std::vector<std::unique_ptr<Vertex>> vertices;
    std::vector<std::unique_ptr<Edge>>   edges;

    int     max_num_vertices;
    int     num_vertices;
    int64_t num_edges;

    // method
    int searchHashTable(std::string key);

protected:
    // property
    std::vector<int> vertex_hash_table;
    int hash_table_size;

    // method
    void readFromFile(std::string net_file, std::string mode);
    int addVertex(char* key);
    void insertHashTable(char* key, int new_v_id);
    u_int Hash(std::string skey);

};

#endif //PTESELF_NETWORK_H
