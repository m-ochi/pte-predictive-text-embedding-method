//
// Created by m-ochi on 16/03/15.
//

#ifndef PTESELF_EMBEDDEDVECTOR_H
#define PTESELF_EMBEDDEDVECTOR_H

#include <iostream>
#include <vector>

typedef float real;                    // Precision of float numbers

class EmbeddedVector {
public:
    EmbeddedVector(int num_vertices_a, int dim_a);
    ~EmbeddedVector();
    std::vector<std::vector<real>>  vertex;
    std::vector<std::vector<real>>  context;
    int num_vertices;
    int dim;

private:
    std::vector<std::vector<real>> InitEmb2DArray(int num_vertices, int dim);

};


#endif //PTESELF_EMBEDDEDVECTOR_H
