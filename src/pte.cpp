#include "pte.h"
#include <mutex>

//#define MAX_STRING 100
//#define SIGMOID_BOUND 6
//#define NEG_SAMPLING_POWER 0.75

typedef float real;                    // Precision of float numbers

static std::mutex mtx;
std::unique_lock<std::mutex> uniq_lk(mtx);

// new
std::unique_ptr<ConstantSettings> PTE::constant_settings;

// 定数群

std::unique_ptr<Network> PTE::network_ww;
std::unique_ptr<Network> PTE::network_wd;
std::unique_ptr<Network> PTE::network_wl;
std::unique_ptr<Network> PTE::network_all;

std::unique_ptr<Samplers> PTE::sampler_ww;
std::unique_ptr<Samplers> PTE::sampler_wd;
std::unique_ptr<Samplers> PTE::sampler_wl;

std::unique_ptr<EmbeddedVector> PTE::emb_ww;
std::unique_ptr<EmbeddedVector> PTE::emb_wd;
std::unique_ptr<EmbeddedVector> PTE::emb_wl;
std::unique_ptr<EmbeddedVector> PTE::emb_all;

std::unique_ptr<Sigmoid> PTE::sigmoid;

int64_t PTE::total_samples;

int64_t PTE::current_sample_count;

real PTE::cur_rho;

PTE::PTE(
        std::string ww_net_a,
        std::string wd_net_a,
        std::string wl_net_a,
        std::string outputfile_a,
        int binary_a,
        int size_a,
        int order_a,
        int negative_a,
        int samples_a,
        int threads_a,
        float rho_a
)
{

    // 定数群
    constant_settings = std::unique_ptr<ConstantSettings>( new ConstantSettings(ww_net_a,wd_net_a,wl_net_a,outputfile_a,binary_a,size_a,order_a,negative_a,samples_a,threads_a,rho_a) );

    current_sample_count = 0;
    cur_rho = constant_settings->rho;

}

PTE::~PTE() {
}

void PTE::run() {

    total_samples = this->constant_settings->samples * 1000000;

    if (constant_settings->order != 1 && constant_settings->order != 2) {
        printf("Error: order should be eighther 1 or 2!\n");
        exit(1);
    }
    printf("--------------------------------\n");
    printf("Order: %d\n", constant_settings->order);
    printf("Samples: %lldM\n", total_samples / 1000000);
    printf("Negative: %d\n", constant_settings->negative);
    printf("Dimension: %d\n", constant_settings->dim);
    printf("Initial rho: %lf\n", constant_settings->rho);
    printf("--------------------------------\n");

    // networkの作成
    std::vector<std::string> networkfiles;
    networkfiles.push_back(this->constant_settings->ww_net);
    networkfiles.push_back(this->constant_settings->wd_net);
    networkfiles.push_back(this->constant_settings->wl_net);

    network_ww = std::unique_ptr<Network>(new Network(networkfiles[0], "ww"));
    network_wd = std::unique_ptr<Network>(new Network(networkfiles[1], "wd"));
    network_wl = std::unique_ptr<Network>(new Network(networkfiles[2], "wl"));
    network_all = std::unique_ptr<Network>(new NetworkAll(networkfiles));

    // samplerの作成
    sampler_ww = std::unique_ptr<Samplers>(new Samplers(network_ww.get()));
    sampler_wd = std::unique_ptr<Samplers>(new Samplers(network_wd.get()));
    sampler_wl = std::unique_ptr<Samplers>(new Samplers(network_wl.get()));

    // embeddingの作成
    emb_ww = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_ww->num_vertices, this->constant_settings->dim));
    emb_wd = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_wd->num_vertices, this->constant_settings->dim));
    emb_wl = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_wl->num_vertices, this->constant_settings->dim));
    emb_all = std::unique_ptr<EmbeddedVector>(new EmbeddedVector(network_all->num_vertices, this->constant_settings->dim));

    sigmoid = std::unique_ptr<Sigmoid>(new Sigmoid());

    std::cout << "total_samples:" << total_samples << std::endl;

    clock_t start;
    start = clock();
    printf("--------------------------------\n");

    // マルチプロセス
    std::thread* th = new std::thread[constant_settings->threads];
    for (int i=0; i< constant_settings->threads; i++) {
        th[i] = std::thread(&PTE::trainPTEThread, i);
    }

    for (int i=0; i < constant_settings->threads; i++) {
        th[i].join();
    }
    std::cout << "complete" << std::endl;

    std::cout << std::endl;
    clock_t finish = clock();

    std::cout << "Total time: " << (double)(finish - start) / CLOCKS_PER_SEC << std::endl;

    this->output();
}

/* Update embeddings */
void PTE::update(std::vector<real>& vec_u, std::vector<real>& vec_v, std::vector<real>& vec_error, int label) {
    // update の式にw_ijのエッジの重みがついてないのは,サンプルを繰り返すうちに重みの分の比で更新されていくという発想だと思われる.
    real x = 0;
    real g;


    //内積の計算
    for (int c = 0; c != constant_settings->dim; c++) {
        x += vec_u[c] * vec_v[c];
    }

    g = - (label - sigmoid->fastFunc(x)) * cur_rho;
    for (int c = 0; c != constant_settings->dim; c++) {
        vec_error[c] += g * vec_v[c];
    }

    for (int c = 0; c != constant_settings->dim; c++) {
        vec_v[c] -= g * vec_u[c];
    }
}

void PTE::trainPTEThread(int id) {
    int64_t count = 0;
    int64_t last_count = 0;
    uint64_t seed = (uint64_t) (int64_t)id;

    while (1) {
        // judge for exit
        // total_samples=1Mがベース
        if (count > total_samples / constant_settings->threads + 2) {
            break;
        }

        // 表示の調整
        if (count - last_count>9999) {
//            std::unique_lock<std::mutex> uniq_lk(mtx);
            current_sample_count += count - last_count;
            last_count = count;
            printf("%cRho: %f  Progress: %.3lf%%", 13, cur_rho, (real)current_sample_count / (real)(total_samples + 1) * 100);
            fflush(stdout);
            // rho:学習率,init_rho=0.025サンプル数に応じて下げている
            cur_rho = constant_settings->rho * (1 - current_sample_count / (real)(total_samples + 1));
            // rhoの下限設定
            if (cur_rho < constant_settings->rho * 0.0001) {
                cur_rho = constant_settings->rho * 0.0001;
            }
        }

        // joint training
        Network* n_ww = network_ww.get();
        Network* n_wd = network_wd.get();
        Network* n_wl = network_wl.get();
        Network* n_all = network_all.get();

        EmbeddedVector* e_ww = emb_ww.get();
        EmbeddedVector* e_wd = emb_wd.get();
        EmbeddedVector* e_wl = emb_wl.get();
        EmbeddedVector* e_all = emb_all.get();
        Samplers* s_ww = sampler_ww.get();
        Samplers* s_wd = sampler_wd.get();
        Samplers* s_wl = sampler_wl.get();

        learnVector(n_ww, e_ww, s_ww, n_all, e_all, seed);
        learnVector(n_wd, e_wd, s_wd, n_all, e_all, seed);
        learnVector(n_wl, e_wl, s_wl, n_all, e_all, seed);

        count++;

    }
}

void* PTE::learnVector(
    Network*         network_each,
    EmbeddedVector*  emb_each,
    Samplers*        sampler_each,
    Network*         network_all_a,
    EmbeddedVector*  emb_all_a,
    uint64_t seed
) {

    int64_t curedge;
    int64_t s_id;
    int64_t t_id;
    std::string s_name;
    std::string t_name;

    int64_t s_all_id;
    int64_t t_all_id;
    std::string s_all_name;
    std::string t_all_name;

    std::vector<real> vec_error(constant_settings->dim);


    // edgeサンプリング（エッジのWeightに比例する形でAliasTableを利用してサンプリング）
    // for ww network
    curedge = sampler_each->edgeSampling();
    s_id    = network_each->edges[curedge]->source_id;
    t_id    = network_each->edges[curedge]->target_id;


    //s_idとt_idをランダムに入れ替える仕様に変更してみる
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> r_uni(0, 1.0);
    double rand = r_uni(mt);
    if(rand<0.5) {
        int64_t tmp_id = s_id;
        s_id = t_id;
        t_id = tmp_id;
    }

    s_name  = network_each->vertices[s_id]->name;
    t_name  = network_each->vertices[t_id]->name;

    s_all_id    = network_all_a->searchHashTable(s_name);
    t_all_id    = network_all_a->searchHashTable(t_name);
    s_all_name  = network_all_a->vertices[s_all_id]->name;
    t_all_name  = network_all_a->vertices[t_all_id]->name;

    // copy latest emb_all to emb_each
    for (int c = 0; c != constant_settings->dim; c++) {
        emb_each->vertex[s_id][c] = emb_all_a->vertex[s_all_id][c];
    }

    // vec_error, vec_error_allを０で初期化
    for (int c = 0; c != constant_settings->dim; c++) {
        vec_error[c] = 0;
    }

    int64_t target;
    int64_t label;
    // NEGATIVE SAMPLING
    for (int d = 0; d != constant_settings->negative + 1; d++) {
        if (d == 0) {
            target = t_id;
            label = 1;
        } else {
            target = sampler_each->negativeVertexSampling(seed);
            t_name = network_each->vertices[target]->name;
            t_all_id = network_all->searchHashTable(t_name);
            label = 0;

        }

        // copy latest emb_all to emb_each
        for (int c = 0; c != constant_settings->dim; c++) {
            emb_each->vertex[target][c]     = emb_all_a->vertex[t_all_id][c];
            emb_each->context[target][c]    = emb_all_a->context[t_all_id][c];
        }

        // 更新
        if (constant_settings->order == 1) {
            update(emb_each->vertex[s_id], emb_each->vertex[target], vec_error, label);
            for (int c = 0; c != constant_settings->dim; c++) {
                emb_all_a->vertex[t_all_id][c] = emb_each->vertex[target][c];
            }
        }


        if (constant_settings->order == 2) {
            update(emb_each->vertex[s_id], emb_each->context[target], vec_error, label);
            for (int c = 0; c != constant_settings->dim; c++) {
                emb_all_a->context[t_all_id][c] = emb_each->context[target][c];
            }
        }


    }

    for (int c = 0; c != constant_settings->dim; c++) {
        // original LINE code is mistaken perhaps, change + to -.
        emb_each->vertex[s_id][c] -= vec_error[c];
        emb_all_a->vertex[s_all_id][c] = emb_each->vertex[s_id][c];
    }
}

void PTE::output() {
    FILE* fo = fopen(this->constant_settings->outputfile.c_str(), "wb");
    fprintf(fo, "%d %d\n", network_all->num_vertices, this->constant_settings->dim);
    for (int i = 0; i < network_all->num_vertices; i++) {
        std::string name = this->network_all->vertices[i]->name;
        fprintf(fo, "%s ", name.c_str());
        if (this->constant_settings->binary) {
            for (int j = 0; j < this->constant_settings->dim; j++) {
                fwrite(&emb_all->vertex[i][j], sizeof(real), 1, fo);
            }

        } else {
            for (int j = 0; j < this->constant_settings->dim; j++) {
                fprintf(fo, "%lf ", emb_all->vertex[i][j]);
            }
        }

        fprintf(fo, "\n");
    }
    fclose(fo);
}

void PTE::output_tmp(int loop_c) {
    std::unique_lock<std::mutex> uniq_lk(mtx);
    std::string outputfile_n = constant_settings->outputfile + std::to_string(loop_c);
    FILE* fo = fopen(outputfile_n.c_str(), "wb");
    fprintf(fo, "%d %d\n", network_all->num_vertices, constant_settings->dim);
    for (int i = 0; i < network_all->num_vertices; i++) {
        std::string name = network_all->vertices[i]->name;
        fprintf(fo, "%s ", name.c_str());
        if (constant_settings->binary) {
            for (int j = 0; j < constant_settings->dim; j++) {
                fwrite(&emb_all->vertex[i][j], sizeof(real), 1, fo);
            }

        } else {
            for (int j = 0; j < constant_settings->dim; j++) {
                fprintf(fo, "%lf ", emb_all->vertex[i][j]);
            }
        }

        fprintf(fo, "\n");
    }
    fclose(fo);
}

void PTE::disp_vec(std::vector<real> vec) {

    int vec_size = vec.size();
    std::cout << "vec::";
    for(int i=0; i<vec_size; i++) {
        if(i>0) {
            std::cout << ", ";
        }
        std::cout << vec[i];

    }
    std::cout << std::endl;

}
