//
// Created by m-ochi on 16/03/11.
//

#include "constantsettings.h"

ConstantSettings::ConstantSettings(
        std::string ww_net_a,
        std::string wd_net_a,
        std::string wl_net_a,
        std::string outputfile_a,
        int binary_a,
        int dim_a,
        int order_a,
        int negative_a,
        int samples_a,
        int threads_a,
        float rho_a
)
:
        ww_net(ww_net_a),
        wd_net(wd_net_a),
        wl_net(wl_net_a),
        outputfile(outputfile_a),

        binary(binary_a),
        dim(dim_a),
        order(order_a),
        negative(negative_a),
        samples(samples_a),
        threads(threads_a),
        rho(rho_a)
{

    hash_table_size = 30000000;
    neg_table_size = 1e8;
    sigmoid_table_size = 1000;

}


ConstantSettings::~ConstantSettings() {}

/*
int main() {

    ConstantSettings* cs = new ConstantSettings("dataminimal/st-st.csv","dataminimal/st-corp.csv","dataminimal/st-imp.csv","vec_2nd_wo_norm.txt");
    std::unique_ptr<ConstantSettings> cs1( new ConstantSettings("dataminimal/st-st.csv","dataminimal/st-corp.csv","dataminimal/st-imp.csv","vec_2nd_wo_norm.txt") );

    std::cout << "cs->ww_net:"              << cs->ww_net               << std::endl;
    std::cout << "cs->sigmoid_table_size:"  << cs->sigmoid_table_size   << std::endl;
    std::cout << "cs1->neg_table_size:"     << cs1->neg_table_size      << std::endl;
    std::cout << "cs1->sigmoid_table_size:" << cs1->sigmoid_table_size  << std::endl;

    delete cs;

    return 0;
}
*/

