//
// Created by m-ochi on 16/03/11.
//

#ifndef PTESELF_VERTEX_H
#define PTESELF_VERTEX_H

#include <iostream>
#include <memory>

class Vertex {
public:
    Vertex();
    ~Vertex();
    double degree;
    std::string name;
};

#endif //PTESELF_VERTEX_H
