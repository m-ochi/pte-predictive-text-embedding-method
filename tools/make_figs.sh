#!/bin/bash

#header="../results/early_lock2d100k/vec_2nd_wo_norm.txt"
header="../src/vec_2nd_wo_norm.txt"
times=10000
#nmax=70
nmax=46

for i in `seq 0 ${nmax}`
do
    n=`expr ${i} \* ${times}`
    file=${header}${n}
    echo $file
    python2.7 make_tsne_fig.py ${file} ${i}
done

