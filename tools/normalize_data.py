#!/usr/local/bin/python
# -*- coding: utf-8 -*-

'''
Created on Mar 08, 2016
データをソース側(最初の列)でNormalizeする

@author: ochi
'''

import csv
import collections as cl
import codecs
import sys


def run(a_file):
    sum_dic = cl.defaultdict(float)
    all_dic = {}
    f = open(a_file, 'r')
    reader = csv.reader(f, delimiter='\t')
    for row in reader:
        s_id = row[0]
        t_id = row[1]
        w = float(row[2])
        if s_id not in all_dic.keys():
            all_dic[s_id] = {}

        if t_id in all_dic[s_id].keys():
            print "%s->%s is exist."%(s_id, t_id)
        else:
            all_dic[s_id][t_id] = w
            sum_dic[s_id] += w

    f.close()

    for s_id in all_dic.keys():
        for t_id in all_dic[s_id].keys():
            p = all_dic[s_id][t_id] / sum_dic[s_id]
            if p > 1e-6:
                print '"%s" "%s" %f'%(s_id, t_id, p)


if __name__ == "__main__":
    a_file = sys.argv[1]
    run(a_file)
