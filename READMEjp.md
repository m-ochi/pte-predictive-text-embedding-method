### PTE(Predictive Text Embedding)法
--------------------------------

##### Introduction
- ヘテロジニアス（異種ノード混合）ネットワークを同一のベクトル空間へ写像する手法
- 語の共起ネットワーク，語と文書ＩＤネットワーク，語と語のカテゴリ情報ネットワークの３種類のネットワークを元に同時に学習する．
- 語のカテゴリ情報が全体の一部にしかついていなくても，半教師あり学習のフレームワークでラベル伝搬をさせることが可能になる．

- 元論文[PTE](http://dl.acm.org/citation.cfm?id=2783307&CFID=567858599&CFTOKEN=79641490)(PTE: Predictive Text Embedding through Large-scale Heterogeneous Text Networks） KDD'15
- ベースとなるソースコードは[LINE](https://github.com/tangjianpku/LINE)

##### ディレクトリ構成
- dataminimal: サンプル実行用データ
- src:ソースコード＋コンパイルサンプル実行用シェルスクリプト
- tools: tsneによる獲得ベクトルの可視化コードなど，おまけ

##### コンパイル方法
- cmakeを使います．

```
cd src/
git submodule init
git submodule update
cmake .
make
```

##### 特徴
- C++11の機能を使っています
- AliasTableによるサンプリング高速化
- HogWild!法によるASGDの実装（並列化）

##### サンプルデータ実行方法
```
cd src/
./run.sh
```
##### train_pteのオプション
-  --train_ww=./data/st-st.csv : 単語ー単語ネットワークのファイル
-  --train_wd=./data/st-corp.csv：単語ー文書ネットワークのファイル
-  --train_wl=./data/st-imp.csv：単語ーラベルネットワークのファイル
-  --output=vec_2nd_wo_norm.txt：出力ファイル名
  --binary=0：バイナリで出力（デバッグしてないので使えないかも）
-  --size=128：写像するベクトルの次元
-  --order=2：１次のつながりか二次のつながりのどちらかの類似度をベースに学習する(PTEでは二次のつながりのみを利用する)
-  --negative=5：負例サンプリングの数
-  --samples=30：サンプリングの回数(\*10^6です)
-  --threads=30：並列ＣＰＵの数
-  --rho=0.05：学習率（だんだん低下していきます）

```./train_pte --help```で詳細なオプションがみれます

##### 出力ファイルのフォーマット
- １行目：頂点数 次元数
- ２行目以降："頂点名" 各次元の値
- すべて半角スペース区切り

##### 開発状況
- 初期版リリース
- 現状はJoint Trainingのみ
    - 単語ー単語で少し学習-> 単語ー文書で少し学習 -> 単語ーラベルで少し学習をサンプリング回数分繰り返す．

##### 感想
- 精度検証は十分できていないです
- 手法のコンセプトとしては，ソースをいじればいくつでもネットワークを増やせる点がおもしろい．



